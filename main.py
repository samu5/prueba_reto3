# This is a sample Python script.
import numpy as np
# Press Mayús+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def balotera(balotas):
    np.random.shuffle(balotas)

    #maximos     = [5,5,4,5,5]
    #contadores  = [0,0,0,0,0]
    #tam_maximo  = sum(maximos)

    #minimas_balotas = len(balotas)

    boletas_escogidas =[]
    #if minimas_balotas >= 24:
    for index, bolata in enumerate(balotas):
        #num_random   =np.random.randint(0,len(balotas))
        balota_sacada = balotas[index]

        if balota_sacada.startswith("B"):
            if contadores[0] < maximos[0]:
                boletas_escogidas.append(balota_sacada)
                contadores[0] = contadores[0] + 1

        elif balota_sacada.startswith("I"):
            if contadores[1] < maximos[1]:
                boletas_escogidas.append(balota_sacada)
                contadores[1] = contadores[1] + 1

        elif balota_sacada.startswith("N"):
            if contadores[2] < maximos[2]:
                boletas_escogidas.append(balota_sacada)
                contadores[2] = contadores[2] + 1

        elif balota_sacada.startswith("G"):
            if contadores[3] < maximos[3]:
                boletas_escogidas.append(balota_sacada)
                contadores[3] = contadores[3] + 1


        elif balota_sacada.startswith("0"):
            if contadores[4] < maximos[4]:
                boletas_escogidas.append(balota_sacada)
                contadores[4] = contadores[4] + 1


        if len(boletas_escogidas) == tam_maximo:
            break
        balotas_revueltas = tuple(boletas_escogidas)
       # else:
            #balotas_revueltas = tuple(balotas)


        return balotas_revueltas

def prueba_balotera(balotas):
    np.random.shuffle(balotas)
    cnt_extraidas = {"B":0,"I":0,"N":0,"G":0,"O":0}
    boletas_escogidas = []

    for balota in balotas:
        if cnt_extraidas["B"] >= 5 and cnt_extraidas["I"] >= 5 and cnt_extraidas["N"] >= 4 and cnt_extraidas["G"] >= 5 and cnt_extraidas["O"] >= 5:
            break
        boletas_escogidas.append(balota)
        cnt_extraidas[balota[0]] += 1
    balotas_revueltas = tuple(boletas_escogidas)

    return balotas_revueltas









# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    balotas = ["B1", "B2", "B3","B4","B5","B6","B7","B8","B9","B10","B11","B12","B13","B14", "B15", "I16", "I17", "I18","I19"
    ,"I17","I18","I19","I20","I21","I22","I23","I24","I25","I26","I27","I28","I29", "I30",
    "N31", "N32", "N33","N34","N35","N36","N37","N38","N39","N40","N41","N42","N43","N44", "N45", "G46"
    , "G47", "G48", "G49", "G50", "G51", "G52", "G53", "G54", "G55", "G56", "G57", "G58", "G59", "G60", "O61", "O62",
    "O63","O64","O65","O66","O67","O68","O69","O70","O71","O72","O73","O74" "O75"]
    print(prueba_balotera(balotas))

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
